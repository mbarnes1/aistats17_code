library(ggplot2)
n_vec = seq(from = 100, to = 100, length = 11)
betas = seq(from = 0.2, to = 0.2, length = 11)
thetas = seq(from = 0.0, to=1.0, length = 11)
bound_expected_error = c(NaN, -1.1845589, -0.8241017, -0.6132478, -0.4636445, -0.3476032, -0.4636445, -0.6132478, -0.8241017, -1.1845589, NaN)
error_exactsampling = c(0.99, 0.97, 0.94, 0.95, 0.85, 0.96, 0.93, 0.88, 0.95, 0.96, 1.00)
error_gibbs = c(0.99, 0.98, 0.97, 0.93, 0.92, 0.93, 0.96, 0.93, 0.93, 0.97, 1.00)
description = '10000 Gibbs sampler, holding latent individuals at true values entire time'

save(n_vec, betas, bound_expected_error, error_exactsampling, error_gibbs, description, file=paste('results/',paste(format(Sys.time(), "%Y-%m-%d-%H-%M-%S"), "Rdata", sep = "."), sep=''))
